import NavMenu from "./NavMenu";
import NavLink from "./NavLink";

export default {
  NavMenu,
  NavLink
};