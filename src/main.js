import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "vue2-animate/dist/vue2-animate.min.css";
import VueNumerals from "vue-numerals";
import checkView from "vue-check-view";

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.performance = true;
Vue.use(VueNumerals);
Vue.use(checkView);
Vue.prototype.$lodash = require("lodash");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
