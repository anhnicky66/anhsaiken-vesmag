import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home";
import Contact from "./views/Contact";
import Categories from "./views/Categories";
import ProductList from "./views/ProductList";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/categories",
      name: "categories",
      component: Categories
    },
    {
      path: "/categories/:categoryId",
      name: "productList",
      component: ProductList
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    console.log(to, from, savedPosition);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 500)
    })
  }
});
